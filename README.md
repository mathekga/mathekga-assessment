This project uses Spark, the Java minimalistic Web framework, to provide the requested API.

# How to build

$ docker build -t assessment-image .

# The initial build will take a while because it is installing dependencies

# How to run the container

$ docker run -i -t --rm --name assessment-container -p 8080:8080 assessment-image


To run it in detached mode:

$ docker run -d --rm --name assessment-container -p 8080:8080 assessment-image


# How to post with content type

Make a "POST" request to [http://localhost:8080/create](http://localhost:8080/create) 
with `Content-Type: application/json` and a body as follows:
{
  "path": "/the/path/to/the/directory"
}


