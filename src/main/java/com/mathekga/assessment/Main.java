package com.mathekga.assessment;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Response;

import java.io.IOException;
import java.nio.file.*;
import java.util.Map;

import static spark.Spark.*;

public class Main {

    private String pathField = "path";

    private String endPoint = "/create";

    public static void main(String[] args) {

        // Initialize our server as requested
        port(8080);

        init();

        // Our endpoint
        post(endPoint, (req, res) -> {
            // JSON response
            res.type("application/json");

            //Check if content JSON exist
            if (!req.contentType().equals("application/json")) {
                res.status(400);
                return "{\"error\":\"Please POST with application/json as content type\"}";
            }

            String body = req.body() ? req.body() : "{}" ;

            JsonValue bodyValue = Json.parse(body);

            if (!bodyValue.isObject()) {
                res.status(400);
                return String.format("{\"error\":\"Please post a JSON object with a property named %s\"}", pathField);
            }

            // Get the path from posted data
            String filePath = val.asObject().get(pathField).asString();

            // Respond according to filepath and responseErrors
            try {
                return getFilePath(filePath);
            } catch (IOException ex) {
                return responseErrors(ex, res);
            }
        });
    }

    /**
     * Builds a Json array of the provided directory's files and their information
     * @param path The path of the directory
     * @return A Json array with the information requested
     * @throws IOException In case there's any IO error
     */
    private static JsonValue getFilePath(String path) throws IOException {

        Path directory = Paths.get(path);

        DirectoryStream<Path> dirStream = Files.newDirectoryStream(directory);

        JsonArray jsonArrayAnswer = new JsonArray();

        for (Path pp : dirStream) {

            JsonObject file = new JsonObject();

            file.add("path", pp.toString());
            file.add("size", Files.size(pp));

            Map<String, Object> attributes = Files.readAttributes(pp, "posix:*");

            for (Map.Entry<String, Object> entry : attributes.entrySet()) {
                file.add(entry.getKey(), entry.getValue().toString());
            }

            jsonArrayAnswer.add(file);
        }

        return jsonArrayAnswer;
    }

    /**
     The error message in JSON format
     */
    private static String responseErrors(Exception ex, Response res) {

        String errorMessage;
        if (ex instanceof NoSuchFileException) {
            errorMessage = "The specified path does not exist in the filesystem";
            res.status(404);
        } else if (ex instanceof AccessDeniedException) {
            errorMessage = "Access is denied at the filesystem level";
            res.status(403);
        } else if (ex instanceof NotDirectoryException) {
            errorMessage = "Not a directory";
            res.status(403);
        } else {
            errorMessage = ex.getMessage();
            res.status(500);
        }

        return String.format("{\"unexpected_error\":\"%s\"}", errorMessage);
    }


}
