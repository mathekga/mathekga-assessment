FROM openjdk

ENV DEBIAN_FRONTEND=noninteractive

# Variables for paths inside the image
ARG BIN_DIR=/usr/local/bin
ARG APP_DIR=/app


WORKDIR $BIN_DIR
ARG TINI_VERSION=0.16.1

RUN set -ex \
    && buildDeps=' \
            wget \
        ' \
	&& apt-get update && apt-get install -y $buildDeps maven --no-install-recommends && rm -rf /var/lib/apt/lists/* \
	&& wget -O tini "https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini" \
	&& chmod +x * \
	&& apt-get purge -y --auto-remove $buildDeps


WORKDIR $APP_DIR

COPY pom.xml .
RUN ["mvn", "dependency:resolve"]
RUN ["mvn", "verify"]

# Build artifacts
ADD src $APP_DIR/src
RUN ["mvn", "package"]

# Execution
EXPOSE 8080
ENTRYPOINT ["/usr/local/bin/tini", "-vv", "--"]
CMD ["java", "-classpath" , "target/dependency/*:target/mathekga-submission-1.0.jar", "com.mathekga.assessment.Main"]
